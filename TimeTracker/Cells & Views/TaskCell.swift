//
//  TaskCell.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import UIKit

class TaskCell: UITableViewCell{
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    //MARK -- Timer
    var timer: Timer?
    var currentRunningTask: Task?
    var seconds: Int = 0
    var runningTask: Bool = false
    var updateTaskModelElapsedTime: ((Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func killTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func startCounting() {
        killTimer()
        runningTask = true
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(self.updateValue),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func updateValue() {
        seconds += 1
        timerLabel.text = timeString(time: TimeInterval(seconds))
        updateTaskModelElapsedTime?(seconds)
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return "\(hours):\(minutes):\(seconds)"
    }
}

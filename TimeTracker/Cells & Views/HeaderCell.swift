//
//  HeaderCell.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import UIKit

class HeaderCell: UITableViewCell{
    
    @IBOutlet weak var titleLabel: UILabel!
    var runningRow: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

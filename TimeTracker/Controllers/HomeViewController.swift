//
//  ViewController.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tasksTableView: UITableView!
    var inputTextField: UITextField?
    
    let viewModel: HomeViewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        /*
         Add observers to detect app termination/enterBackground states
         To save data before close or minimizing tha app
        */
        addAppTerminateAndBackgroundObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*
         Save all data before leaving the view
        (case will not happpen as we have no other views)
        */
        viewModel.saveBeforeExit()
        //Remove all observers
        removeTerminateObserver()
    }
    
    func setupView() {
        viewModel.getTasks()
        tasksTableView.reloadData()
    }
    
    @objc func saveDataBeforeClose() {
         viewModel.saveBeforeExit()
    }
    
    //Observe to app WillTerminate / DidEnterBackground
    func addAppTerminateAndBackgroundObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(saveDataBeforeClose),
                                               name: .UIApplicationWillTerminate,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(saveDataBeforeClose),
                                               name: .UIApplicationDidEnterBackground,
                                               object: nil)
    }
    
    //Remove observers
    func removeTerminateObserver() {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
    }
    
    //Add new developer action
    @IBAction func addDeveloperAction(_ sender: UIButton) {
        constructAlertWithTextField()
    }
    
    //MARK -- Alert with textField
    func constructAlertWithTextField() {
        let alert = UIAlertController(title: viewModel.alertTitle, message: viewModel.alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        
        inputTextField?.placeholder = viewModel.alertTextFieldPlaceHolder
        alert.addTextField { [weak self] textField -> Void in
            self?.inputTextField = textField
        }
        
        alert.addAction(UIAlertAction(title: viewModel.alertDoneButtonTitle, style: .default, handler: { [weak self] action in
            guard let `self` = self else { return }
            switch action.style{
            case .default:
                self.alertDoneAction()
            default:
                print("none")
            }
        }))
        
        alert.addAction(UIAlertAction(title: viewModel.alertCancelButtonTitle, style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Alert done confirming adding new developer
    func alertDoneAction() {
        if let count = self.inputTextField?.text?.count,
            count > 0,
            let developerName = self.inputTextField?.text{
                viewModel.addNewDeveloper(developerName: developerName)
                setupView()
        }
    }
}


// MARK : Delegate and Datasource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellInstance(tableView: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewModel.sectionInstance(tableView: tableView,for: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell: TaskCell = tableView.cellForRow(at: indexPath) as? TaskCell {
            viewModel.cellClicked(tableView: tableView, tableViewCell: cell, at: indexPath)
        }
    }
}

//
//  HomeViewModel.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/15/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import UIKit

class HomeViewModel: HomeCellsRepresentable, HomeConstants {
    
    var tasksModel: DevelopersTasksModel?
    var numberOfSections: Int = 0
    
    var alertTitle: String = "New Developer"
    var alertMessage: String = "Please enter developer name"
    var alertTextFieldPlaceHolder: String = "Developer Name"
    var alertDoneButtonTitle: String = "Done"
    var alertCancelButtonTitle: String = "Cancel"
    
    //MARK -- Get tasks list
    func getTasks() {
        tasksModel = ServiceManager.readJsonData()
        numberOfSections = tasksModel?.developers?.count ?? 0
    }
    
    //MARK -- return number of rows in section
    func numberOfRowsInSection(section: Int) -> Int {
        return tasksModel?.developers?[section].tasks?.count ?? 0
    }
    
    //MARK -- return cell (TaskCell / NoTaskCell)
    func cellInstance(tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return constructNoTasksLabel(tableView: tableView, at: indexPath)
        default:
            return constructTaskWithTimerLabel(tableView: tableView, at: indexPath)
        }
    }
    
    //MARK -- return section view (HeaderCell)
    func sectionInstance(tableView: UITableView, for section: Int) -> UITableViewCell {
        return constructSection(tableView:tableView, at: section)
    }
    
    //MARK -- Adding a new developer
    func addNewDeveloper(developerName: String) {
        if let model = tasksModel {
            ServiceManager.addDeveloper(name: developerName, allTasksModel: model)
        }
    }
    
}

extension HomeViewModel {
    /*
     cell clicked -- Run cell task timer
     Params:
        tableView -- tasks tableView
        tableViewCell -- clicked cell
        indexPath -- indexPath of running task cell
     */
    func cellClicked(tableView: UITableView, tableViewCell: TaskCell, at  indexPath: IndexPath) {
        // If taks is running,,, return
        if tasksModel?.developers?[indexPath.section].tasks?[indexPath.row].running == true { return }
        // If Developer has running task, pause it!
        if (tasksModel?.developers?[indexPath.section].runningTask) != nil {
            //Get Developer running task
            //Loop through section (developer) tasks
            if let enumerated = tasksModel?.developers?[indexPath.section].tasks?.enumerated() {
                for (taskRow, task) in enumerated {
                    //If this is current running task:
                    if task.running == true {
                        let runningTaskIndexPath = IndexPath(row: taskRow, section: indexPath.section)
                        if let cell: TaskCell = tableView.cellForRow(at: runningTaskIndexPath) as? TaskCell {
                            pauseTask(task: task, tableViewCell: cell, at: runningTaskIndexPath)
                        }
                    }
                }
            }
        }
        if let taskToRun = tasksModel?.developers?[indexPath.section].tasks?[indexPath.row] {
            runTask(task: taskToRun, taskCell: tableViewCell, at: indexPath)
        }
    }
    
    /*
     Pause task
     Params:
        task -- task model to pause
        tableViewCell -- cell of running task
        indexPath -- indexPath of running task cell
     */
    func pauseTask(task: Task, tableViewCell: TaskCell, at  indexPath: IndexPath) {
        print("will pause task : \(task.title ?? "") for developer : \(tasksModel?.developers?[indexPath.section].name ?? "")")
        tasksModel?.developers?[indexPath.section].runningTask = nil
        task.running = false
        tableViewCell.backgroundColor = UIColor.clear
        tableViewCell.killTimer()
    }
    
    /*
     run task
     Params:
         task -- task model to pause
         tableViewCell -- cell of running task
         indexPath -- indexPath of running task cell
     */
    func runTask(task: Task, taskCell: TaskCell, at  indexPath: IndexPath) {
        print("will run task : \(task.title ?? "") for developer : \(tasksModel?.developers?[indexPath.section].name ?? "")")
        task.running = true
        tasksModel?.developers?[indexPath.section].runningTask = task
        taskCell.runningTask = true
        taskCell.backgroundColor = UIColor.lightGray
        taskCell.startCounting()
    }
    
    /*
     save current developers tasks state before app close
     */
    func saveBeforeExit() {
        guard let model = tasksModel else { return }
        pauseAllTasks()
        FilesManager.writeAllTasksToFile(tasksModel: model)
    }
    
    /*
     pause all running tasks
     */
    func pauseAllTasks() {
        tasksModel?.developers?.forEach({ (developer) in
            developer.runningTask = nil
            developer.tasks?.forEach({ (task) in
                task.running = false
            })
        })
    }
}

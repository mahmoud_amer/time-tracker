//
//  HomeProtocols.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/15/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import UIKit

protocol HomeCellsRepresentable {
    var numberOfSections : Int {get}
    func numberOfRowsInSection(section: Int) -> Int
    func cellInstance(tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell
    func sectionInstance(tableView: UITableView, for section: Int) -> UITableViewCell
}

protocol HomeConstants {
    var alertTitle: String {get}
    var alertMessage: String {get}
    var alertTextFieldPlaceHolder: String {get}
    var alertDoneButtonTitle: String {get}
    var alertCancelButtonTitle: String {get}
}

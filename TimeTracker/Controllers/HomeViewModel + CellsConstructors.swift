//
//  CellsConstructors.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/16/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import UIKit

//MARK -- Cells/Sections Constructors
extension HomeViewModel {
    //MARK -- Task Cell
    func constructTaskWithTimerLabel(tableView: UITableView, at  indexPath: IndexPath) -> TaskCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TaskCell",for: indexPath) as! TaskCell
        if let task = tasksModel?.developers?[indexPath.section].tasks?[indexPath.row] {
            //We must clear the timer to avoid dequeueReusableCell "SCROLLING" issues
            cell.killTimer()
            cell.backgroundColor = UIColor.clear
            cell.titleLabel.text = task.title ?? ""
            cell.timerLabel.text = cell.timeString(time: TimeInterval(task.elapsedTime ?? 0))
            cell.seconds = task.elapsedTime ?? 0
            cell.updateTaskModelElapsedTime = { [weak self] (elapsedTime) in
                //Update Task Model with elapsed time
                self?.tasksModel?.developers?[indexPath.section].tasks?[indexPath.row].elapsedTime = elapsedTime
            }
            if let running = task.running, running == true {
                runTask(task: task, taskCell: cell, at: indexPath)
            }
        }
        return cell
    }
    
    //MARK -- Section (Developer Name)
    func constructSection(tableView: UITableView, at  section: Int) -> HeaderCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        cell.accessibilityIdentifier = tasksModel?.developers?[section].name
        cell.titleLabel.text = tasksModel?.developers?[section].name
        return cell
    }
    
    //MARK -- No Tasks Cell
    func constructNoTasksLabel(tableView: UITableView, at  indexPath: IndexPath) -> NoTasksCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "NoTasksCell",for: indexPath) as! NoTasksCell
        cell.titleLabel.text = "No Tasks"
        return cell
    }
}

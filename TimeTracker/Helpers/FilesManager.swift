//
//  FileManager.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation

class FilesManager: NSObject {
    internal static var jsonFileName: String {
        return "timetracker.json"
    }
    
    /*
     Check if json file exists
     If not, Create new onw
     */
    public static func validateJsonFile() {
        if !checkIfJsonFileExists() {
            createProjectFirectoryInDocuments()
            if createEmptyFile() {
                let firstModel: DevelopersTasksModel = ServiceManager.createFirstStateModel()
                if let jsonString = firstModel.toJSONString() {
                    saveJsonFile(jsonString: jsonString)
                    print("file created successfully")
                } else {
                    print("Couldn't create string from DevelopersTasksModel")
                    //todo -- handle error
                }
            } else {
                print("Couldn't create empty file")
                //todo -- handle error
            }
        }
    }
}

extension FilesManager {
    //Check if json file exist
    private static func checkIfJsonFileExists() -> Bool {
        let destinationUrl = getDocumentsDirectory().appendingPathComponent(jsonFileName)
        print("destinationUrl : \(destinationUrl)")
        return FileManager.default.fileExists(atPath: destinationUrl.path)
    }
    
    //Create documents directory
    private static func createProjectFirectoryInDocuments() {
        do {
            try FileManager.default.createDirectory(
                atPath: getDocumentsDirectory().path,
                withIntermediateDirectories: true,
                attributes: nil)
        } catch let error as NSError {
            print("Error creating documents folder: \(error.localizedDescription)")
        }
    }
    
    //Create empty file
    private static func createEmptyFile() -> Bool {
        let fileUrl = getDocumentsDirectory().appendingPathComponent(jsonFileName)
        return FileManager.default.createFile(atPath: fileUrl.path, contents: nil, attributes: nil)
    }
    
    ///Save json string to the file
    static func saveJsonFile(jsonString: String) {
        
        let jsonFileURL = getDocumentsDirectory().appendingPathComponent(jsonFileName)
        do {
            try jsonString.write(to: jsonFileURL, atomically: true, encoding: String.Encoding.utf8)
            print("JSON data was written to teh file successfully! \(jsonFileURL)")
        } catch let error as NSError {
            print("Couldn't write to file: \(error.localizedDescription)")
        }
    }
    
    ///get documents directory as URL
    private static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

//Read JSON file
extension FilesManager {
    //Read file content and return it as string
    public static func readFileContent() -> String? {
        do {
            let jsonFileURL = getDocumentsDirectory().appendingPathComponent(jsonFileName)
            let data = try Data(contentsOf: jsonFileURL, options: [])
            return String(data: data, encoding: String.Encoding.utf8)
        } catch {
            print(error)
            return nil
        }
    }
    
    //Save current tasks model to the file as json string
    public static func writeAllTasksToFile(tasksModel: DevelopersTasksModel) {
        if let jsonString: String = tasksModel.toJSONString() {
            saveJsonFile(jsonString: jsonString)
        }
    }
}

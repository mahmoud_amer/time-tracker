//
//  ServiceManager + Constants.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/16/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation

//Constants
extension ServiceManager {
    //creating developer for first time app runs
    static func createDeveloperFrom(number: Int) -> Developer {
        let developer = Developer()
        developer.name = getDeveloperName(number: number)
        developer.tasks = getDeveloperTasks()
        return developer
    }
    
    //creating developer for "Adding New Developer"
    static func createDeveloperFrom(name: String) -> Developer {
        let developer = Developer()
        developer.name = name
        developer.tasks = getDeveloperTasks()
        return developer
    }
    
    static func getDeveloperTasks() -> [Task] {
        var tasks: [Task] = []
        for i in 0..<ConstantsInt.numberOfTasks.rawValue {
            tasks.append(getTask(number: i))
        }
        return tasks
    }
    
    static func getDeveloperName(number: Int) -> String {
        switch number {
        case 0:
            return ConstantsString.Dmitry.rawValue
        case 1:
            return ConstantsString.Alexandr.rawValue
        case 2:
            return ConstantsString.Pavel.rawValue
        default:
            return ConstantsString.Pavel.rawValue
        }
    }
    
    static func getTask(number: Int) -> Task {
        let task: Task = Task()
        switch number {
        case 0:
            task.title = ConstantsString.NoTask.rawValue
            task.elapsedTime = 0
            return task
        case 1:
            task.title = ConstantsString.DinnerRestTime.rawValue
            task.elapsedTime = 0
            return task
        case 2:
            task.title = ConstantsString.Development.rawValue
            task.elapsedTime = 0
            return task
        case 3:
            task.title = ConstantsString.Bugfix.rawValue
            task.elapsedTime = 0
            return task
        case 4:
            task.title = ConstantsString.Refactoring.rawValue
            task.elapsedTime = 0
            return task
        default:
            task.title = ConstantsString.Refactoring.rawValue
            task.elapsedTime = 0
            return task
        }
    }
}

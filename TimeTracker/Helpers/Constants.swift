//
//  Constants.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/16/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation

public enum ConstantsInt: Int {
    case numberOfTasks = 5
    case numberOfDevelopers = 3
}

public enum ConstantsString: String {
    case NoTask = "No Task"
    case DinnerRestTime = "Dinner/rest time"
    case Development
    case Bugfix
    case Refactoring
    case Dmitry
    case Alexandr
    case Pavel
}

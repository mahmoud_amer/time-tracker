//
//  ServiceManager.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/15/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import ObjectMapper

class ServiceManager {
    static var numberOfTasks: Int = 5
    static var numberOfDevelopers: Int = 3
    /*
     Create dictionary of all developers with tasks and save them
     as json string to the file
     */
    static func createFirstStateModel() -> DevelopersTasksModel {
        
        var developers: [Developer] = []
        for i in 0..<ConstantsInt.numberOfDevelopers.rawValue {
            let developer: Developer = createDeveloperFrom(number: i)
            developers.append(developer)
        }
        let timeTrackModel: DevelopersTasksModel = DevelopersTasksModel()
        timeTrackModel.developers = developers
        return timeTrackModel
    }
    
    //MARK -- Read json file and convert content string to DevelopersModel
    static func readJsonData() -> DevelopersTasksModel? {
        if let jsonString: String = FilesManager.readFileContent() {
            let timeTrackModel = DevelopersTasksModel(JSONString: jsonString)
            return timeTrackModel
        }
        return nil
    }
    
    /*
     Add developer to json file
     Params: current Model data to override
        1- create dictionary with developer name and tasks
        2- map dictionary to Developer model
        3- append developer to old developers in model
        4- Save the model as json string to the file
     */
    static func addDeveloper(name: String, allTasksModel: DevelopersTasksModel) {
        let developer = createDeveloperFrom(name: name)
        allTasksModel.developers?.append(developer)
        if let jsonString = allTasksModel.toJSONString() {
            FilesManager.saveJsonFile(jsonString: jsonString)
        }
    }
}

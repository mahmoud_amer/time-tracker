//
//  TimeTrackerModel.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import ObjectMapper

class DevelopersTasksModel: BaseModel {
    public var developers: [Developer]?
    
    override public func mapping(map: Map) {
        developers <- map["developers"]
    }
}

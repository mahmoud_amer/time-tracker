//
//  BaseModel.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import ObjectMapper

open class BaseModel: NSObject, Mappable, NSCoding {
    
    public required init?(map: Map) {}
    public override init() {}
    open func mapping(map: Map) {}
    
    open func encode(with aCoder: NSCoder) { }
    public required init?(coder aDecoder: NSCoder) { }
}

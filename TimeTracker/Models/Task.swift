//
//  Task.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import ObjectMapper

class Task: BaseModel {
    public var title: String?
    public var elapsedTime: Int?
    public var running: Bool?
    
    override public func mapping(map: Map) {
        title <- map["title"]
        elapsedTime <- map["elapsedTime"]
        running <- map["running"]
    }
}

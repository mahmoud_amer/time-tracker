//
//  Person.swift
//  TimeTracker
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import ObjectMapper

class Developer: BaseModel {
    public var name: String?
    public var tasks: [Task]?
    public var runningTask: Task?
    
    override public func mapping(map: Map) {
        name <- map["name"]
        tasks <- map["tasks"]
    }
}

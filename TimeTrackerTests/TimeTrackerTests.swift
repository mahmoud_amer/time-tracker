//
//  TimeTrackerTests.swift
//  TimeTrackerTests
//
//  Created by Mahmoud Amer on 8/14/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import TimeTracker


class TimeTrackerTests: XCTestCase {
    class FilesManagerTests: XCTestCase {
        func test_create_first_state_Model() {
            let modelToValidate: DevelopersTasksModel = ServiceManager.createFirstStateModel()
            
            let validatorModel: DevelopersTasksModel = DevelopersTasksModel()
            var developers: [Developer] = []
            for developerIndex in 0..<ConstantsInt.numberOfDevelopers.rawValue {
                let developer = Developer()
                switch developerIndex {
                case 0:
                    developer.name = ConstantsString.Dmitry.rawValue
                case 1:
                    developer.name = ConstantsString.Alexandr.rawValue
                case 2:
                    developer.name = ConstantsString.Pavel.rawValue
                default:
                    developer.name = ConstantsString.Dmitry.rawValue
                }
                developer.tasks = []
                for taskIndex in 0..<ConstantsInt.numberOfTasks.rawValue {
                    let task = Task()
                    switch taskIndex {
                    case 0:
                        task.title = ConstantsString.NoTask.rawValue
                    case 1:
                        task.title = ConstantsString.DinnerRestTime.rawValue
                    case 2:
                        task.title = ConstantsString.Development.rawValue
                    case 3:
                        task.title = ConstantsString.Bugfix.rawValue
                    case 4:
                        task.title = ConstantsString.Refactoring.rawValue
                    default:
                        task.title = ConstantsString.Refactoring.rawValue
                    }
                    task.elapsedTime = 0
                    developer.tasks?.append(task)
                }
                developers.append(developer)
            }
            validatorModel.developers = developers
            print(modelToValidate)
            
            XCTAssertEqual(validatorModel.toJSONString(), modelToValidate.toJSONString())
        }
        
        func test_add_developer() {
            let newDeveloper = Developer()
            newDeveloper.name = "added developer"
            newDeveloper.tasks = []
            for taskIndex in 0..<ConstantsInt.numberOfTasks.rawValue {
                let task = Task()
                switch taskIndex {
                case 0:
                    task.title = ConstantsString.NoTask.rawValue
                case 1:
                    task.title = ConstantsString.DinnerRestTime.rawValue
                case 2:
                    task.title = ConstantsString.Development.rawValue
                case 3:
                    task.title = ConstantsString.Bugfix.rawValue
                case 4:
                    task.title = ConstantsString.Refactoring.rawValue
                default:
                    task.title = ConstantsString.Refactoring.rawValue
                }
                task.elapsedTime = 0
                newDeveloper.tasks?.append(task)
            }
            
            let validatorModel: DevelopersTasksModel = ServiceManager.readJsonData()!
            validatorModel.developers?.append(newDeveloper)
            
            let currentModel = ServiceManager.readJsonData()
            ServiceManager.addDeveloper(name: "added developer", allTasksModel: currentModel!)
            let modelToValidate: DevelopersTasksModel = ServiceManager.readJsonData()!
            
            XCTAssertEqual(validatorModel.toJSONString(), modelToValidate.toJSONString())
            
        }
    }
}
